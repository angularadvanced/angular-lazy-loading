import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './modules/general/home/home.component';
import { NotFoundComponent } from './modules/general/not-found/not-found.component';
import {ContactComponent} from './modules/general/contact/contact.component';
import {AboutComponent} from './modules/general/about/about.component';
import {LoginComponent} from './modules/general/login/login.component';
import {SignupComponent} from './modules/general/signup/signup.component';

const routes: Routes = [
  { path: '', component: HomeComponent, },
  {
    path: 'contact',
    loadChildren: () => import('./modules/general/contact/contact.module')
      .then(mod => mod.ContactModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./modules/general/about/about.module')
      .then(mod => mod.AboutModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./modules/general/login/login.module')
      .then(mod => mod.LoginModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./modules/general/signup/signup.module')
      .then(mod => mod.SignupModule)
  },
  { path: '**', component: NotFoundComponent }
];

/*const routes: Routes = [
  { path: '', component: HomeComponent, },
  {
    path: 'contact',
    children: [{
      path: '',
      component: ContactComponent
    }]
  },
  {
    path: 'about',
    children: [{
      path: '',
      component: AboutComponent
    }]
  },
  {
    path: 'login',
    children: [{
      path: '',
      component: LoginComponent
    }]
  },
  {
    path: 'signup',
    children: [{
      path: '',
      component: SignupComponent
    }]
  },
  { path: '**', component: NotFoundComponent }
];*/

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
